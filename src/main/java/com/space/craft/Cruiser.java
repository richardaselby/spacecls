package com.space.craft;

import com.space.tech.Weapon;

/**
 * Cruiser Class
 * 
 * @author Richard Selby
 * @version (a version number or a date)
 */
public class Cruiser extends Ship
{
    private Weapon specialWeapon = null;


    public Weapon getSpecialWeapon() {
        return specialWeapon;
    }

    public void setSpecialWeapon(Weapon specialWeapon) {
        if (specialWeapon.getTechlevel().getLevel() >=30){
            this.specialWeapon = specialWeapon;
        }

    }





    /**
     * Constructor for objects of class Cruiser
     */
    public Cruiser()
    {
        this.setTopSpeed(100);
        //this.setShieldStrength(50);
        //this.hullStrength(250);
    }

    /**
     * An example of a method - replace this comment with your own
     * 
     * @return     the sum of x and y
     */
    public int getAttackValue()
    {
        // put your code here
        return -1;
    }
    public int getDefenceValue()
    {
        // put your code here
        return -1;
    }
}
