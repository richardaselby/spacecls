package com.space.craft;

import java.util.*;
import com.space.tech.Weapon;
/**
 * Abstract class Ship - write a description of the class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class Ship
{
    // instance variables - replace the example below with your own
    private int topSpeed;
    private int shieldStrength;
    private int hullStrength;

    private int hullHealth;
    private List<Weapon> weapons = new ArrayList<Weapon>();
    
    public abstract int getDefenceValue();
    public abstract int getAttackValue();
    
    public int getTopSpeed() {
        return topSpeed;
    }
    
    public void setTopSpeed(int topSpeed) {
        this.topSpeed = topSpeed;
    }

    public int getHullHealth() {
        return hullHealth;
    }

    public void setHullHealth(int hullHealth) {
        this.hullHealth = hullHealth;
    }
}
