package com.space.tech;

import com.space.tech.Weapon;

/**
 * Write a description of class Rifle here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Rifle extends Weapon
{
    /**
     * Constructor for objects of class Rifle
     */
    public Rifle()
    {
        this.setPower(4);
    }

}
