package com.space.tech;
/**
 * Enumeration class TechLevel - write a description of the enum class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
public enum TechLevel
{
    TIER1(10), TIER2(20), WEDNESDAY(30), THURSDAY(40), FRIDAY(50), SATURDAY(60), SUNDAY(70);

    private TechLevel(int level){
        this.level = level;
    }

    private int level;
    public int getLevel() {
        return level;
    }


}
