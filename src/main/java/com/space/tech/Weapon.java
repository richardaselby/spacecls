package com.space.tech;
/**
 * Abstract class Weapon - write a description of the class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class Weapon
{
    // instance variables - replace the example below with your own
    private int power;
    private TechLevel techlevel = null;

    public TechLevel getTechlevel() {
        return techlevel;
    }

    public void setTechlevel(TechLevel techlevel) {
        this.techlevel = techlevel;
    }





    public int getPower() {return power;}
    public void setPower(int power) {this.power = power;}
}
